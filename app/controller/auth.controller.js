const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.userregister = (req, res) => {
  const user = new User({
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    nic: req.body.nic,
    dateofbirth: req.body.dateofbirth,
    email: req.body.email,
    address: req.body.address,
    gender: req.body.gender,
    phonenumber: req.body.phonenumber,
    password: bcrypt.hashSync(req.body.password, 8),
    roles: "user",
  });
  user.save((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    } else {
      res.send({ message: "User Succusfully Sign up" });
    }
  });
};

exports.signin = (req, res) => {
  User.findOne({
    email: req.body.email,
  }).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }
    if (!user) {
      return res.status(404).send({ message: "User Not found." });
    }
    var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
    if (!passwordIsValid) {
      return res.status(401).send({
        accessToken: null,
        message: "Invalid Password!",
      });
    }
    var token = jwt.sign({ id: user.id }, config.secret, {
      expiresIn: 86400, // 24 hours
    });
    var authorities = user.roles;
    res.status(200).send({
      id: user._id,
      firstname: user.firstname,
      lastname: user.lastname,
      nic: user.nic,
      email: user.email,
      dateofbirth: user.dateofbirth,
      address: user.address,
      gender: user.gender,
      phonenumber: user.phonenumber,
      roles: authorities,
      accessToken: token,
    });
  });
};

exports.edituser = (req, res) => {
  User.updateOne(
    { _id: req.body.id },
    {
      $set: {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        nic: req.body.nic,
        dateofbirth: req.body.dateofbirth,
        email: req.body.email,
        address: req.body.address,
        gender: req.body.gender,
        phonenumber: req.body.phonenumber,
        roles: req.body.roles,
      },
    },
    function (err, result) {
      if (err) {
        res.send({ err });
      }
      res.send({ message: "User Successfully updated" });
    }
  );
};

exports.alluser = async (req, res) => {
  console.log(req.body);
  await User.findOne({ nic: req.body.searchnic })
    .then((user) => {
      console.log(user);
      res.status(200).json(user);
    })
    .catch((err) => {
      res.status(400).json({
        err: err,
      });
    });
};
