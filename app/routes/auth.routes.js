const { verifySignUp, authJwt } = require("../middlewares");
const controller = require("../controller/auth.controller");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
  app.post(
    "/api/auth/userregister",
    [verifySignUp.checkDuplicateNicOrEmail, verifySignUp.matchRegisterPassword],
    controller.userregister
  );
  app.post("/api/auth/userlogin", controller.signin);
  app.post("/api/auth/edituser", [authJwt.verifyToken], controller.edituser);
  app.post("/api/auth/alluser",[authJwt.isAdmin], controller.alluser);
};
