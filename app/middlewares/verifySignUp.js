const db = require("../models");
const User = db.user;

checkDuplicateNicOrEmail = (req, res, next) => {
  // Nic
  User.findOne({
    nic: req.body.nic,
  }).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }
    if (user) {
      res.status(400).send({ message: "Failed! NIC is already in use!" });
      return;
    }
    // Email
    User.findOne({
      email: req.body.email,
    }).exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }
      if (user) {
        res.status(400).send({ message: "Failed! Email is already in use!" });
        return;
      }
      next();
    });
  });
};

matchRegisterPassword = (req, res, next) => {
  if (req.body.password !== req.body.confirmpassword) {
    res.status(400).send({
      message: `Password and Confirm password does not match`,
    });
    return;
  }
  next();
};

const verifySignUp = {
  checkDuplicateNicOrEmail,
  matchRegisterPassword,
};
module.exports = verifySignUp;
