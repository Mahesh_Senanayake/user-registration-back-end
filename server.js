const express = require("express");
const cors = require("cors");
const dbConfig = require("./app/config/db.config");
var bcrypt = require("bcryptjs");

const app = express();

var corsOptions = {
  origin: "http://localhost:3000",
};

app.use(cors(corsOptions));

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

const db = require("./app/models");
const User = db.user;

db.mongoose
  .connect(`mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Successfully connect to MongoDB.");
    initial();
  })
  .catch((err) => {
    console.error("Connection error", err);
    process.exit();
  });

app.get("/", (req, res) => {
  res.json({ message: "Welcome to register application." });
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

require("./app/routes/auth.routes")(app);

function initial() {
  var query = User.find({ roles: "admin" });
  query.count(function (err, count) {
    if (err) console.log(err);
    else {
      if (count === 0) {
        const user = new User({
          firstname: "",
          lastname: "",
          nic: "",
          dateofbirth: "",
          email: "admin.userregister@gmail.com",
          address: "",
          gender: "",
          phonenumber: "",
          password: bcrypt.hashSync("admin123", 8),
          roles: "admin",
        });
        user.save((err, user) => {
          if (err) {
            console.log(err);
            return;
          } else {
            console.log("Admin user create");
          }
        });
      }else console.log("Admin create before")
    }
  });
}
