const mongoose = require("mongoose");

const User = mongoose.model(
  "User",
  new mongoose.Schema({
    firstname: String,
    lastname: String,
    nic: String,
    dateofbirth: String,
    email: String,
    address: String,
    gender: String,
    phonenumber: String,
    password: String,
    roles: String,
  })
);

module.exports = User;
